Rails.application.routes.draw do
  resources :games, only: [:index, :create, :show], param: :slug do
    resources :guesses, only: [:index, :create]

    member do
      get 'results'
    end
  end
end
