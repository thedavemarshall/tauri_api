class GamesChannel < ApplicationCable::Channel
  def subscribed
    @game = Game.find_by(slug: params[:slug])
    stream_for @game
    GamesChannel.broadcast_to(@game, @game.to_h)
  end

  def unsubscribed
    # will need to suspend any unfinished games in multiplayer if one player disconnects
  end
end
