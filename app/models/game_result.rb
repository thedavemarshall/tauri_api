class GameResult < ApplicationRecord
  belongs_to :game

  def self.build_from_game(game)
    return nil unless game.finished?

    guess_times = game.guesses.map(&:created_at)
    return game.build_game_result({
      seconds_elapsed: (guess_times.max - guess_times.min).to_i,
      guess_count: game.guesses.count
    })
  end

  # returns seconds_elapsed: INTEGER and time_string: STRING
  def to_h
    guess_times = game.guesses.map(&:created_at)
    {
      seconds_elapsed: self.seconds_elapsed,
      time_string: seconds_in_words(self.seconds_elapsed),
      guess_count: self.guess_count
    }
  end


  # https://apidock.com/rails/v6.0.0/ActionView/Helpers/DateHelper/distance_of_time_in_words
  # Credit to Rails ActionView::Helpers for distance_of_time_in_words(), which was repurposed into seconds_in_words()
  # original source: actionview/lib/action_view/helpers/date_helper.rb

  def seconds_in_words(distance_in_seconds, options={})
    distance_in_minutes = (distance_in_seconds / 60.0).round

    I18n.with_options locale: options[:locale], scope: options[:scope] || [:datetime, :distance_in_words] do |locale|
      case distance_in_minutes
      when 0..1
        return distance_in_minutes == 0 ?
               locale.t(:less_than_x_minutes, count: 1) :
               locale.t(:x_minutes, count: distance_in_minutes) unless options[:include_seconds]

        case distance_in_seconds
        when 0..4   then locale.t :less_than_x_seconds, count: 5
        when 5..9   then locale.t :less_than_x_seconds, count: 10
        when 10..19 then locale.t :less_than_x_seconds, count: 20
        when 20..39 then locale.t :half_a_minute
        when 40..59 then locale.t :less_than_x_minutes, count: 1
        else             locale.t :x_minutes,           count: 1
        end

      when 2...45           then locale.t :x_minutes,      count: distance_in_minutes
      when 45...90          then locale.t :about_x_hours,  count: 1
        # 90 mins up to 24 hours
      when 90...1440        then locale.t :about_x_hours,  count: (distance_in_minutes.to_f / 60.0).round
        # 24 hours up to 42 hours
      when 1440...2520      then locale.t :x_days,         count: 1
        # 42 hours up to 30 days
      else locale.t :x_days,         count: (distance_in_minutes.to_f / 1440.0).round
      end
    end
  end
end
