class Guess < ApplicationRecord
  belongs_to :game
  after_create :publish_game_state

  def to_h
    {
      word: self.word,
      oxen: self.exact_matches,
      cows: self.wrong_positions
    }
  end

  private
  # This publishes the full game state for each new guess, which is wasteful. Investigate using Redux and only publishing the new data
  def publish_game_state
    GamesChannel.broadcast_to(self.game, self.game.to_h)
  end
end
