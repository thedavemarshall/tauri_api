class Game < ApplicationRecord
  has_many :guesses
  has_one :game_result

  SLUG_LENGTH = 6
  DEFAULT_SOLUTION_LENGTH = 4
  # solution_length => redis set key
  SOLUTION_LENGTH_MAP = {
    4 => '4letterwords',
    5 => '5letterwords',
    6 => '6letterwords',
    7 => '7letterwords',
    8 => '8letterwords'
  }.freeze

  after_validation(on: :create) do
    self.slug ||= generate_slug
    if SOLUTION_LENGTH_MAP[self.solution_length.to_i].blank?
      self.solution_length = DEFAULT_SOLUTION_LENGTH
    end
    self.solution ||= (generate_solution || backup_generate_solution)
    self.solution_length = self.solution.length
  end

  ALPHA_NUMERIC = (('a'..'z').to_a | ('A'..'Z').to_a | (0..9).to_a).freeze
  def generate_slug
    random_string = ALPHA_NUMERIC.sample(SLUG_LENGTH).join
    while Game.exists?(slug: random_string)
      Rails.logger.info("slug generation collision, trying again")
      random_string = ALPHA_NUMERIC.sample(SLUG_LENGTH).join
    end
    random_string
  end

  def generate_solution
    return Redis.current.srandmember(SOLUTION_LENGTH_MAP[self.solution_length.to_i])
  rescue => e
    Rails.logger.error("Error generating game solution from Redis: #{e}")
    return nil
  end

  # This method takes in parameters for a guess, and returns a Guess object with the appropriate scoring and relational information set.
  # note that this method does NOT save to the database, it only initializes a Guess
  def score(guess_params)
    guess = self.guesses.build(guess_params)

    if guess.word.chars.length != solution.chars.length
      guess.errors.add(:word, :invalid, message: "guess word must match the number of characters in the solution", required_length: solution.chars.length)
      return guess
    end

    # count number of exact matches, where the letter and placement in the solution and guess match
    # the indexes are used for exclusion checks in wrong_positions calculations
    exact_matching_indexes = solution.chars.each_with_index.inject([]) do |memo, (solution_char, i)|
      if (guess.word.chars[i] == solution_char)
        memo << i
      end
      memo
    end

    guess.exact_matches = exact_matching_indexes.count

    # excluding exact matches, this counts how often each character is present in the guess and solution, and then find the overlap
    # guess_counts and solution_counts are of the form {CHARACTER => COUNT}
    guess_counts = character_counts(guess.word, exact_matching_indexes)
    solution_counts = character_counts(solution, exact_matching_indexes)

    guess.wrong_positions = guess_counts.map do |guess_char, guess_count|
      [guess_count, solution_counts[guess_char]].min
    end.sum

    return guess
  end

  def character_counts(word, exclusion_indexes)
    word.chars.each_with_index.inject(Hash.new(0)) do |memo, (char, i)|
      unless (exclusion_indexes.include?(i))
        memo[char] += 1
      end
      memo
    end
  end

  WORDLIST_FILENAME = "words.txt".freeze
  def backup_generate_solution
    Rails.logger.error("Using backup game solution generation from words.txt")
    wordlist = File.readlines(WORDLIST_FILENAME)
    word = wordlist[rand(wordlist.length)].strip
  end

  # Get the required attributes, include associated guesses, and transform them to a ruby hash
  # Useful before serializing to json
  # note that it obfuscates the solution, so is safe to pass to clients
  def to_h
    {
      solution_length: solution_length,
      slug: slug,
      guesses: guesses.order("created_at DESC").map(&:to_h)
    }
  end

  def finished?
    self.guesses.any? {|g| g.exact_matches == self.solution_length }
  end
end
