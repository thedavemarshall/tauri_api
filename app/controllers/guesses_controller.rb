class GuessesController < ApplicationController
  def index
    @game = Game.find_by_slug(params[:game_slug])

    if @game.present?
      render json: @game.to_h.to_json
    else
      render json: {}.to_json, status: 404
    end
  end


  # note that if this is successful, it returns the full state of the Game, not just the newly created guess
  def create
    @game = Game.find_by_slug(params[:game_slug])

    if @game.present? # TODO server side uniqueness check here, render appropriate error code
      @guess = @game.score(word: clean_guess_word(guess_params[:word]))
      if @guess.errors.blank? && @guess.save
        render json: @guess.game.to_h.to_json
      else
        error_string = @guess.errors.to_s
        Rails.logger.error("Error creating Guess: #{error_string}")
        render json: {guess: guess_params, error_string: error_string}.to_json, status: 500
      end
    else
      render json: {}.to_json, status: 404
    end
  end

  private
  def guess_params
    params.require(:game_guess).permit(:word)
  end

  def clean_guess_word(word)
    word.downcase.gsub(/[^a-z]/,'')
  end
end
