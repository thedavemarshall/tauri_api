class GamesController < ApplicationController
  def index
    render json: { solution_length: Game::SOLUTION_LENGTH_MAP.keys }.to_json
  end

  def create
    build_params = params.require(:game).permit(:solution_length)
    @game = Game.new(build_params)

    if @game.save
      render json: @game.to_h.to_json
    else
      error_string = @game.errors.join(';')
      Rails.logger.error("Error saving new Game: #{error_string}")
      render json: {game: @game.to_h, error_string: error_string}.to_json, status: 500
    end
  end

  def show
    @game = Game.find_by_slug(params[:slug])

    if @game.present?
      render json: @game.to_h.to_json
    else
      render json: {}.to_json, status: 404
    end
  end

  def results
    @game = Game.find_by_slug(params[:slug])
    render(json: {}, status: 204) unless @game.finished?

    if @game.present?
      @game.game_result ||= GameResult.build_from_game(@game)
      if @game.game_result.save
        render json: @game.game_result.to_h.to_json
      else
        error_string = @game.game_result.errors.join(';')
        Rails.logger.error("Error getting results")
        render json: {game_result: @game.game_result.to_h, error_string: error_string}.to_json, status: 500
      end
    else
      render json: {}.to_json, status: 404
    end
  end
end
