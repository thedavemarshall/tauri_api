if Redis.current.present?
  begin
    words = File.readlines("/usr/share/dict/words").map(&:strip);
    words_to_use = words.select do |word|
      redis_key = Game::SOLUTION_LENGTH_MAP[word.length]
      if redis_key.present? && (word.length == word.split('').uniq.length) && (word.downcase == word)
        Redis.current.sadd(redis_key, word)
      end
    end
  rescue => e
    puts "Error seeding Redis with game solutions"
    puts e.to_s
  end
else
  puts "Unable to seed Redis- check credentials"
end
