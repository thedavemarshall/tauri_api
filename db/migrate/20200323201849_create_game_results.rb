class CreateGameResults < ActiveRecord::Migration[6.0]
  def change
    create_table :game_results do |t|
      t.references :game, null: false, foreign_key: true
      t.integer :seconds_elapsed, null: false
      t.integer :guess_count, null: false

      t.timestamps
    end
  end
end
