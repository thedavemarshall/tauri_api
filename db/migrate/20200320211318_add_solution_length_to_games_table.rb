class AddSolutionLengthToGamesTable < ActiveRecord::Migration[6.0]
  def up
    add_column :games, :solution_length, :integer

    Game.all.each do |game|
      game.solution_length = game.solution.length
      game.save!
    end

    change_column :games, :solution_length, :integer, null: false
    add_index :games, :solution_length
  end

  def down
    remove_column :games, :solution_length
  end
end
