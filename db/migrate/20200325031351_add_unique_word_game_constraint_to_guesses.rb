class AddUniqueWordGameConstraintToGuesses < ActiveRecord::Migration[6.0]
  def up
    Game.all.each do |game|
      (game.guesses - game.guesses.uniq(&:word)).each do |duplicate_guess|
        duplicate_guess.delete
      end
    end

    add_index :guesses, [:word, :game_id], unique: true
  end

  def down
    remove_index :guesses, column: [:word, :game_id]
  end
end
