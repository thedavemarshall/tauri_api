class CreateGuesses < ActiveRecord::Migration[6.0]
  def change
    create_table :guesses do |t|
      t.references :game, null: false, foreign_key: true
      t.string :word, null: false
      t.integer :exact_matches, null: false
      t.integer :wrong_positions, null: false

      t.timestamps
    end
  end
end
