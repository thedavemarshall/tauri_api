# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_03_25_031351) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "game_results", force: :cascade do |t|
    t.bigint "game_id", null: false
    t.integer "seconds_elapsed", null: false
    t.integer "guess_count", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["game_id"], name: "index_game_results_on_game_id"
  end

  create_table "games", force: :cascade do |t|
    t.string "solution", null: false
    t.string "slug", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "solution_length", null: false
    t.index ["slug"], name: "index_games_on_slug", unique: true
    t.index ["solution_length"], name: "index_games_on_solution_length"
  end

  create_table "guesses", force: :cascade do |t|
    t.bigint "game_id", null: false
    t.string "word", null: false
    t.integer "exact_matches", null: false
    t.integer "wrong_positions", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["game_id"], name: "index_guesses_on_game_id"
    t.index ["word", "game_id"], name: "index_guesses_on_word_and_game_id", unique: true
  end

  add_foreign_key "game_results", "games"
  add_foreign_key "guesses", "games"
end
